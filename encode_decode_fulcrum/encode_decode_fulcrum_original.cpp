// Copyright Steinwurf ApS 2015.
// Distributed under the "STEINWURF EVALUATION LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#include <algorithm>
#include <cstdint>
#include <ctime>
#include <iostream>
#include <vector>

#include <storage/storage.hpp>

#include <kodo_fulcrum/coders.hpp>

#include <kodo_rlnc/coders.hpp>

/// @example encode_decode_fulcrum.cpp
///
/// Simple example showing how to encode and decode a block of data using
/// the Fulcrum codec.
/// For a detailed description of the Fulcrum codec see the following paper
/// on arxiv: http://arxiv.org/abs/1404.6620 by Lucani et. al.

int main_orignial()
{
    // Seed the random number generator to get different random data
    srand(static_cast<uint32_t>(time(0)));

    // Specify the coding parameters
    fifi::finite_field field_binary8 = fifi::finite_field::binary8;
    fifi::finite_field field_binary =  fifi::finite_field::binary;

    uint32_t symbols = 32;
    uint32_t symbol_size = 16;
    // The number of expansion symbols is a unique parameter for fulcrum
    uint32_t expansion = 4;

    // Build an encoder and a decoder
    kodo_fulcrum::encoder encoder(field_binary8, symbols, symbol_size, expansion);
    kodo_fulcrum::decoder decoder(field_binary8, symbols, symbol_size, expansion);

    kodo_fulcrum::decoder recoder(field_binary, symbols, symbol_size, 0);
    

    // Get the number of expansion symbols on the fulcrum encoder
    std::cout << "Expansion symbols on the fulcrum encoder : "
              << encoder.expansion() << std::endl;

    // Get the number of expansion symbols on the fulcrum decoder
    std::cout << "Expansion symbols on the fulcrum decoder : "
              << decoder.expansion() << std::endl;

    // Allocate some storage for a "payload" the payload is what we would
    // eventually send over a network
    std::vector<uint8_t> payload(encoder.max_payload_size());


    // Allocate some data to encode. In this case we make a buffer
    // with the same size as the encoder's block size (the max.
    // amount a single encoder can encode)
    std::vector<uint8_t> data_in(encoder.block_size());

    // Just for fun - fill the data with random data
    std::generate(data_in.begin(), data_in.end(), rand);

    // Assign the data buffer to the encoder so that we may start
    // to produce encoded symbols
    encoder.set_symbols_storage(data_in.data());

    // Define a data buffer where the symbols should be decoded
    std::vector<uint8_t> data_out(decoder.block_size());
    decoder.set_symbols_storage(data_out.data());


    std::vector<uint8_t> data_out_re(recoder.block_size());


    // Generate packets until the decoder is complete
    while (!decoder.is_complete())
    {
        // Encode a packet into the payload buffer
        encoder.produce_payload(payload.data());

        recoder.consume_payload(payload.data());

        // Pass that packet to the decoder
        decoder.consume_payload(payload.data());



    }

    // Check if we properly decoded the data
    if (std::equal(data_out.begin(), data_out.end(), data_in.begin()))
    {
        std::cout << "Data decoded correctly" << std::endl;
    }
    else
    {
        std::cout << "Unexpected failure to decode, "
                  << "please file a bug report :)" << std::endl;
    }
}
