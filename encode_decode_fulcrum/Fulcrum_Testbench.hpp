#pragma once

// #ifndef FULCRUM_TESTBENCH_H
// #define FULCRUM_TESTBENCH_H


#include <unistd.h>
// get the Class TimeCounter
#include "TimeCounter.hpp"
#include <algorithm>
#include <cstdint>
#include <ctime>
#include <iostream>
#include <vector>
#include <string>

#include <storage/storage.hpp>

#include <kodo_fulcrum/coders.hpp>

#include <kodo_rlnc/coders.hpp>

#include <kodo_core/object/file_encoder.hpp>
#include <kodo_core/object/file_decoder.hpp>

#include <fstream>
// #include <jsoncpp/json/json.h>

// #include <vector>

// // #include <jsoncpp/json/json.cpp>
extern std::vector<double> en_throughput_buffer;
extern std::vector<double> re_throughput_buffer;
extern std::vector<double> de_throughput_buffer;

class Fulcrum_Testbench{

    private:

        uint32_t symbols;//= 32;
        uint32_t symbol_size ; //= 1024;
        
        // According K = 1600
        uint32_t packet_size;

        // Fulcrum Recoder setting
        uint32_t recoder_symbols;

        // The number of expansion symbols is a unique parameter for fulcrum
        uint32_t expansion ;//= 4;
        double erasure1; //= 0.1;
        double erasure2; //= 0.1;
        bool show_codeTime_flag = true;
        int RUN_LOOP = 1;

    public:
        Fulcrum_Testbench(){
            symbols= 32;
            symbol_size = 1600;

            packet_size = 1600;
            recoder_symbols = 4;
        // The number of expansion symbols is a unique parameter for fulcrum
            expansion = 4;
            erasure1= 0.1;
            erasure2= 0.1;

            show_codeTime_flag = true;
        }
        Fulcrum_Testbench(uint32_t symbols, uint32_t symbol_size,
                          uint32_t packet_size, uint32_t expansion, 
                          double erasure1,double erasure2):
                          symbols(symbols), symbol_size(symbol_size),
                          packet_size(packet_size),expansion(expansion),
                          erasure1(erasure1),erasure2(erasure2){};

        ~Fulcrum_Testbench(){}

        void set_symbols(uint32_t new_symbols);
        void set_symbol_size(uint32_t new_symbol_size);
        void set_expansion(uint32_t new_expansion);
        void set_erasure(double e1 , double e2);
        void set_show_cT_flag(bool new_flag);
        void set_run_loop(int new_RUN_LOOP);
        void set_packet_size(uint32_t new_set_packet_size);
        void set_recoder_symbols(uint32_t new_recoder_symbols);

        void runOnce();
    
        void run();
};



    
// #endif