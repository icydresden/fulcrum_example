#include "Fulcrum_Testbench.hpp"
// #include <vector>
#include "include/json/json.h"

// // #include <jsoncpp/json/json.cpp>

// extern std::vector<double> en_throughput_buffer;
// extern std::vector<double> re_throughput_buffer;
// extern std::vector<double> de_throughput_buffer;

std::vector<double> en_throughput_buffer;
std::vector<double> re_throughput_buffer;
std::vector<double> de_throughput_buffer;


void Fulcrum_Testbench::set_symbols(uint32_t new_symbols)
{
    this->symbols = new_symbols;
}

void Fulcrum_Testbench::set_symbol_size(uint32_t new_symbol_size){
    this->symbol_size = new_symbol_size;
}
void Fulcrum_Testbench::set_expansion(u_int32_t new_expansion){
    this->expansion = new_expansion;
}
void Fulcrum_Testbench::set_erasure(double e1 , double e2){
    this->erasure1 = e1;
    this->erasure2 = e2;
}
void Fulcrum_Testbench::set_show_cT_flag(bool new_flag){
    // 0 is close
    // 1 is open
    this->show_codeTime_flag = new_flag;
}
void Fulcrum_Testbench::set_run_loop(int new_RUN_LOOP)
{
    this->RUN_LOOP = new_RUN_LOOP;
}

void Fulcrum_Testbench::set_packet_size(uint32_t new_packet_size){
    this->packet_size = new_packet_size;
}

void Fulcrum_Testbench::runOnce()
{
            // Specify the coding parameters
    fifi::finite_field field_binary8 = fifi::finite_field::binary8;
    fifi::finite_field field_binary =  fifi::finite_field::binary;

    // Build an encoder and a decoder
    kodo_fulcrum::encoder encoder(field_binary8, symbols, symbol_size, expansion);
    kodo_fulcrum::decoder decoder(field_binary8, symbols, symbol_size, expansion);


    // using file_encoder = kodo_core::object::file_encoder<kodo_fulcrum::encoder>;
    // using file_decoder = kodo_core::object::file_decoder<kodo_fulcrum::decoder>;

    // file_encoder encoder(field_binary8, symbols, symbol_size, expansion);
    // kodo_fulcrum::decoder recoder(field_binary, symbols, symbol_size, expansion);

    uint32_t recoder_symbols = 4;
    kodo_rlnc::pure_recoder recoder(
        field_binary, symbols + expansion, symbol_size, recoder_symbols);


    // // Get the number of expansion symbols on the fulcrum encoder
    // std::cout << "Expansion symbols on the fulcrum encoder : "
    //           << encoder.expansion() << std::endl;

    // // Get the number of expansion symbols on the fulcrum decoder
    // std::cout << "Expansion symbols on the fulcrum decoder : "
    //           << decoder.expansion() << std::endl;

    // Allocate some storage for a "payload" the payload is what we would
    // eventually send over a network
    std::vector<uint8_t> payload(encoder.max_payload_size());


    // Allocate some data to encode. In this case we make a buffer
    // with the same size as the encoder's block size (the max.
    // amount a single encoder can encode)
     // total packet numbers 1600 8000 16000
    std::vector<uint8_t> data_in(encoder.block_size());
    // std::vector<uint8_t> data_in(packet_size*symbol_size);

    // Just for fun - fill the data with random data
    std::generate(data_in.begin(), data_in.end(), rand);

    // Assign the data buffer to the encoder so that we may start
    // to produce encoded symbols
    // encoder.set_symbols_storage(data_in.data());

    // Define a data buffer where the symbols should be decoded
    // std::vector<uint8_t> data_out(decoder.block_size());

    // std::vector<uint8_t> data_out(packet_size*symbol_size);
    std::vector<uint8_t> data_out(decoder.block_size());


    // decoder.set_symbols_storage(data_out.data());

    // std::vector<uint8_t> data_out_2(recoder.block_size());
    // recoder.set_symbol_storage(data_out_2.data());


    // Generate packets until the decoder is complete
    uint32_t enc_index = 0;
    uint32_t rec_index = 0;
    uint32_t dec_index = 0;

    TimeCounter tcounter;
    long long total_en_time = 0, total_de_time = 0, total_re_time = 0;

    // for (uint32_t i = 0; i < encoder.blocks(); ++i)
    // {
    //     file_encoder::stack_pointer e = encoder.build(i);
    //     file_decoder::stack_pointer d = decoder.build(i);
    //     std::vector<uint8_t> payload(e->max_payload_size());
    std::cout << "Coding time :" <<std::endl;
    
    assert(packet_size >0);
    // packet_size 1600
    // symbols 32
    // uint16_t blocks = ((packet_size % symbols) == 0 ? (packet_size / symbols) : (packet_size % symbols + 1));
    uint16_t blocks = ((packet_size % symbols) == 0 ? (packet_size / symbols) : (packet_size / symbols + 1));


    std::cout << "blocks : " << blocks << std::endl;
    std::cout << "encoder.symbols : " << encoder.symbols() << std::endl;
    std::cout << "encoder.symbol_size : " << encoder.symbol_size() << std::endl;
    std::cout << "encoder.max_payload_size() : " << encoder.max_payload_size() << std::endl;
    std::cout << "encoder.block_size() : " << encoder.block_size() << std::endl;

    auto pData_in = data_in.data();
    auto pData_out = data_out.data();

    std::vector<uint8_t> data_in_temp(encoder.block_size());

    for(int i = 0;i<RUN_LOOP;i++)
    {
        // encoder::buil
        // for(uint16_t j=0;j<blocks; j++)
        // int j = 1;
        {
        // memcpy(& data_in_temp,&data_in + j* symbol_size*symbols, symbol_size*symbols);
        // pData_in += j*symbols;
        // // pData_out += j*symbols;
        // encoder.set_symbols_storage(data_in_temp.data());
        encoder.set_symbols_storage(pData_in);

        decoder.set_symbols_storage(pData_out);

        // encoder.set_symbols_storage(data_in.begin() + j);
        // decoder.set_symbols_storage(data_out.begin() + j);

        while (!decoder.is_complete())
        {
            enc_index ++;
            // Encode a packet into the payload buffer
            tcounter.encoding.start();
            encoder.produce_payload(payload.data());
            tcounter.encoding.stop();
            // std::cout << index <<") Encoded packet generated and received by the recoder =>"
            //           << std::endl << std::endl;
            if ((double) rand() / (RAND_MAX) < erasure1)  //  loss=10%
                {
                    continue;
                }
            
            rec_index ++;
            tcounter.recoding.start();
            recoder.consume_payload(payload.data());

            recoder.produce_payload(payload.data());
            tcounter.recoding.stop();
            // std::cout << "Recoded packet generated and received by the decoder =>"
            //           << std::endl << std::endl;


            if ((double) rand() / (RAND_MAX) < erasure2) // 0 1 2 3 loss=10%
            {
                continue;
            }
            dec_index ++;
            // Pass that packet to the decoder
            tcounter.decoding.start();
            decoder.consume_payload(payload.data());
            tcounter.decoding.stop();

            if(show_codeTime_flag){
                std::cout << "Encoder t = " << tcounter.encoding.time() << " ns "
                      << "Recoder t = " << tcounter.recoding.time() << " ns "
                      << "Decoder t = " << tcounter.decoding.time() << " ns "
                      << "Total time= " << tcounter.total_coding_time() << " ns "
                      << std::endl;
            }
            
                                  
        }
        total_en_time += tcounter.encoding.time();
        total_de_time += tcounter.decoding.time();
        total_re_time += tcounter.recoding.time();
        }
    }

        std::cout << std::endl;
        std::cout << "Chan.1 loss = "<< erasure1*100<<"% | " 
                  << "Chan.2 loss = "<< erasure2*100<<"%" 
                  << "Total pakcets = " << packet_size << " "
                  << "Symbol = " << symbols << " | "
                  << "Symbol_size = " << symbol_size << " "
                  << "Expansion = " << expansion
                  << std::endl;

        // std::cout << "Encoder Throughtput "
        //           << (double) enc_index*(payload.size())*8/tcounter.encoding.time() << " Gb/s " 

        //           << "Encodered Packet = " << enc_index << "\n"

        //           << "Eecoder Throughtput "
        //           << (double) rec_index*(payload.size())*8/tcounter.recoding.time() << " Gb/s "
        //           << "Recodered Packet = " << rec_index << "\n"

        //           << "Decoder Throughtput "
        //           << (double) dec_index*(payload.size())*8/tcounter.decoding.time() << " Gb/s "
        //           << "Decodered Packet = " << dec_index << std::endl

        //           << std::endl;

        double en_through, re_through, de_through;
        en_through = (double) enc_index*(payload.size())*8*RUN_LOOP*1000/total_en_time;
        re_through = (double) rec_index*(payload.size())*8*RUN_LOOP*1000/total_re_time;
        de_through = (double) dec_index*(payload.size())*8*RUN_LOOP*1000/total_de_time;

        std::cout << "Encoder Throughtput "
                //   << (double) enc_index*(payload.size())*8*RUN_LOOP/total_en_time << " Gb/s " 
                  << en_through << " Mb/s " 
                  << "Encodered Packet = " << enc_index << "\n"

                  << "Recoder Throughtput "
                //   << (double) rec_index*(payload.size())*8*RUN_LOOP/total_re_time << " Gb/s "
                  << re_through << " Mb/s " 

                  << "Recodered Packet = " << rec_index << "\n"

                  << "Decoder Throughtput "
                //   << (double) dec_index*(payload.size())*8*RUN_LOOP/total_de_time << " Gb/s "
                  << de_through << " Mb/s " 

                  << "Decodered Packet = " << dec_index << std::endl

                  << std::endl;
        
        en_throughput_buffer.push_back(en_through);
        re_throughput_buffer.push_back(re_through);
        de_throughput_buffer.push_back(de_through);    

    // Check if we properly decoded the data
    if (std::equal(data_out.begin(), data_out.end(), data_in.begin()))
    {
        std::cout << "Data decoded correctly" << std::endl;
    }
    else
    {
        std::cout << "Unexpected failure to decode, "
                  << "please file a bug report :)" << std::endl;
    }


    std::cout << "erasure1 = " << erasure1 << " erasure2 = " << erasure2 
                << " Run_loop = " << RUN_LOOP << " "
                << std::endl; 

    // std::ifstream ifs ("profile.json");

    // Json::Reader reader;
    // Json::Value obj;
    // reader.parse(ifs, obj);     // Reader can also read strings

    // std::cout << "Last name: " << obj["lastname"].asString() << std::endl;
    // std::cout << "First name: " << obj["firstname"].asString() << std::endl;


        
}