#pragma once

#include<chrono>
#include<cassert>
#include<ctime>
#include <sys/times.h>

class TimeCounter{
    using time_chrono = std::chrono::high_resolution_clock;
    private:
        struct TimeContainer
        {
            private:
            
                time_chrono::time_point start_t, stop_t;
                long long period = 0; // long int
            public:
                void start(){
                    start_t = time_chrono::now();
                }

                void stop(){
                    stop_t = time_chrono::now();

                    assert(start_t <= stop_t);
                    period += std::chrono::duration_cast<std::chrono::nanoseconds>(stop_t - start_t).count();
                }

                void clear(){
                    period = 0;
                }

           
                long long time(){
                    return period;
                }
            

                
        };
        
    public:
        TimeContainer encoding;
        TimeContainer decoding;
        TimeContainer recoding;

        TimeContainer coding;
        
        long long total_coding_time(){
            return encoding.time() + decoding.time() + recoding.time();
        }
        
        void clear(){
            encoding.clear();
            decoding.clear();
            recoding.clear();
        }
        
};

class TimeUsed{
    private:
        struct Interval{
            private:
                struct tms start_t, end_t;  // tms: long int (at least 32 bits)
                long s, e;
                long t;
            public:
                void clear(){
                    t = 0;
                }

                void start(){
                    s = times(&start_t);
                }

                void end(){
                    e = times(&end_t);
                    t += e - s;
                }

                double time(){
                    return (double) t/60; //sysconf(_SC_CLK_TCK);
                }
        };
        // double en_de_time;

    public:
        Interval encoding;
        Interval decoding;
        Interval nccoding;
        Interval coding;
    public:
        void clear(){
            encoding.clear();
            decoding.clear();
            nccoding.clear();
            coding.clear();
        }
        double get_en_de_time(){
            return coding.time();
        }
};

class DecoderStatus{
public:
    int nTrans;
    int nReceive;
    int nSave;
    int nError;
    int nInact;
    double * rankdist;

    DecoderStatus(int M){
        rankdist = new double[M+1];
    }
    ~DecoderStatus(){
        delete [] rankdist;
    }
};