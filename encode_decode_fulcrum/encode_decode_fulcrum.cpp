// Copyright Steinwurf ApS 2015.
// Distributed under the "STEINWURF EVALUATION LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing


#include <ctime>
#include <vector>
#include <cstdint>


// get the Class TimeCounter
#include "TimeCounter.hpp"
// #include "BATSimulator.hpp"

#include "Fulcrum_Testbench.hpp"
// #include "Fulcrum_Testbench.cpp"
#include <cstdlib>
#include <iostream>

#include "include/json/json.h"


std::vector<double> en_throughput_buffer;
std::vector<double> re_throughput_buffer;
std::vector<double> de_throughput_buffer;


// int main(int argc, char** argv)
int main(int argc, char** argv)
{
    // Seed the random number generator to get different random data
    srand(static_cast<uint32_t>(time(0)));

    

    // uint32_t symbols = 32 ;//= 32;
    // uint32_t symbol_size; //= //1024; //= 1600;
    // uint32_t packet_size = 1600;
    // // The number of expansion symbols is a unique parameter for fulcrum
    // uint32_t expansion = 4;//= 4;

    uint32_t symbols;
    uint32_t symbol_size; //= //1024; //= 1600;
    uint32_t packet_size;
    // The number of expansion symbols is a unique parameter for fulcrum
    uint32_t expansion; 

    double erasure1 = 0.1;
    double erasure2 = 0.1;
    
  
    const char *optstring = "e:b:c:d:";  
    int opt = -1;
    
    while ((opt = getopt(argc, argv, optstring)) != -1) {
   
    
        switch(opt)
        {
            case 1:
                symbols = 32;
                symbol_size = 1600; //1024; bytes
                packet_size = 1600; // packets
                expansion = 4;
                erasure1 = 0.1;
                erasure2 = 0.1;
                break;
            case 'e':
                symbols = 32;
                symbol_size = 1600;
                packet_size = 1600;
                expansion = 4;
                erasure1 = atof(optarg);
                erasure2 = atof(argv[optind]);
                break;
            // case 3:
            //     // symbols = 32;
            //     // symbol_size = 1600;
            //     // expansion = 4;
            //     erasure1 = atoi(argv[1]);
            //     erasure2 = atoi(optind);
            //     break;
            default:
                std::cout << "The inputs parameters are something wrong !"<<std::endl;
                return 0;
        }
    }
    
    Fulcrum_Testbench fulcrum(symbols,symbol_size,packet_size,expansion,erasure1,erasure2);
    // std::cout << "run fulcrum runOnce()" << std::endl;
    // Fulcrum_Testbench fulcrum;

    fulcrum.set_show_cT_flag(0); // 0 is false
    // fulcrum.set_symbol_size(1024);
    // fulcrum.set_expansion(1);
    // fulcrum.set_erasure(0.1,0.1);
    fulcrum.set_run_loop(10);

    // std::vector<uint32_t> packet_num_arry = {16,32,50,100,500,1000,5000,};
    // for(unsigned int i = 0; i<packet_num_arry.size(); i++)
    // {
    //     fulcrum.set_packet_size(packet_num_arry[i]);
    //     fulcrum.runOnce();

    // }

    // std::cout << "Packet Num:"  << std::endl;
    // for(auto item: packet_num_arry){
    //     std::cout << item << '\t';

    // }


    // std::vector<uint32_t> sybmols_arry = {16,32,160,800,1600,8000,16000};
    uint32_t sybmols_arry []= {16,32,160,800,1600,8000,16000};

    uint32_t expansion_array []= {1,2,3,4};

    for(auto expansion: expansion_array)
    {
        // expansion [1,2,3,4]
        fulcrum.set_expansion(expansion);

        for(auto symbols: sybmols_arry)
        {
            fulcrum.set_symbols(symbols);
            fulcrum.runOnce();

        }
    }
    


    // Represent the Result of the codes
    // symbols number:
    // Encoder Throughtput:
    // Decoder Throughtput:
    std::cout << "Packet Num:"  << std::endl;
    for(auto item: sybmols_arry){
        std::cout << item << '\t';

    }

    std::cout << std::endl;

    std::cout << "Encoder Throughput :" << std::endl;
    for(auto item: en_throughput_buffer){
        std::cout << item << '\t';

    }

    std::cout << std::endl;
    // std::cout << int(en_throughput_buffer.size()) << std::endl;

    std::cout << "Decoder Throughput :" << std::endl;
    for(auto item: de_throughput_buffer){
        std::cout << item << '\t';
    }
    std::cout << std::endl;
    
    // std::cout << "erasure1 =" << erasure1 << " erasure2 =" << erasure2 << std::endl; 
    // std::cout << "finish fulcrum runOnce()" << std::endl;

    return 0;

}