// Copyright Steinwurf ApS 2015.
// Distributed under the "STEINWURF EVALUATION LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#include <algorithm>
#include <cstdint>
#include <ctime>
#include <iostream>
#include <vector>

#include <storage/storage.hpp>

#include <kodo_fulcrum/coders.hpp>

#include <kodo_rlnc/coders.hpp>

#include <kodo_core/object/file_encoder.hpp>
#include <kodo_core/object/file_decoder.hpp>

#include <unistd.h>

// get the Class TimeCounter
#include "TimeCounter.hpp"
// #include "BATSimulator.hpp"

/// @example encode_decode_fulcrum.cpp
///
/// Simple example showing how to encode and decode a block of data using
/// the Fulcrum codec.
/// For a detailed description of the Fulcrum codec see the following paper
/// on arxiv: http://arxiv.org/abs/1404.6620 by Lucani et. al.


// int main(int argc, char** argv)
int main_2(int argc, char** argv)
{
    // Seed the random number generator to get different random data
    srand(static_cast<uint32_t>(time(0)));

    // Specify the coding parameters
    fifi::finite_field field_binary8 = fifi::finite_field::binary8;
    fifi::finite_field field_binary =  fifi::finite_field::binary;

    uint32_t symbols ;//= 32;
    uint32_t symbol_size ; //= 1600;
    // The number of expansion symbols is a unique parameter for fulcrum
    uint32_t expansion ;//= 4;
    double erasure1 = 0.1;
    double erasure2 = 0.1;
    
    // option about the parameters
    int opt;
    int digit_optind = 0; // 设置短参数类型及是否需要参数
    // 如果option_index非空，它指向的变量将记录当前找到参数符合long_opts里的
    // 第几个元素的描述，即是long_opts的下标值
    int option_index = 0;
    // 设置短参数类型及是否需要参数
    int no_argument = 0,required_argument=1,optional_argument=2;

    struct option {
        const char * name;  // 参数的名称
        int has_arg; // 是否带参数值，
        //有三种：no_argument， required_argument，optional_argument
        int * flag; 
        // 为空时，函数直接将 val 的数值从getopt_long的返回值返回出去，
        // 当非空时，val的值会被赋到 flag 指向的整型数中，而函数返回值为0
        int val; // 用于指定函数找到该选项时的返回值，或者当flag非空时指定flag指向的数据的值
    };

    static struct option long_options[] = {
        {"reqarg", required_argument, NULL, 'r'},
        {"noarg",  no_argument,       NULL, 'n'},
        {"optarg", optional_argument, NULL, 'o'},
        {"e1",     optional_argument, NULL, 'e'},
        {0, 0, 0, 0}  // 添加 {0, 0, 0, 0} 是为了防止输入空值
    };

    const char *optstring = "a:b:r:";  

    // const char *optstring = "e1:e2:"; // 有三个选项-abc，其中c选项后有冒号，所以后面必须有参数
    // while ((opt = getopt(argc, argv, optstring)) != -1) {
    while ((opt = getopt(argc, 
                         argv, 
                         optstring,
                         long_options, 
                         &option_index)) != -1) {
    
        switch(opt)
        {
            case 1:
                symbols = 16;
                symbol_size = 1280;
                expansion = 4;
                erasure1 = 0.1;
                erasure2 = 0.1;
                break;
            case 2:
                // symbols = 32;
                // symbol_size = 1600;
                // expansion = 4;
                erasure1 = atoi(argv[1]);
                // erasure2 = 0.1;
                break;
            case 3:
                // symbols = 32;
                // symbol_size = 1600;
                // expansion = 4;
                erasure1 = atoi(argv[1]);
                erasure2 = atoi(argv[2]);
                break;
            default:
                std::cout << "The inputs parameters are something wrong !"<<std::endl;
                return 0;
        }
    }
    // Build an encoder and a decoder
    kodo_fulcrum::encoder encoder(field_binary8, symbols, symbol_size, expansion);
    kodo_fulcrum::decoder decoder(field_binary8, symbols, symbol_size, expansion);

    // kodo_fulcrum::decoder recoder(field_binary, symbols, symbol_size, expansion);
    uint32_t recoder_symbols = 16;
    // kodo_rlnc::pure_recoder recoder(
    //     field_binary, symbols, symbol_size, recoder_symbols);
    kodo_rlnc::decoder recoder(field_binary, symbols, symbol_size);
    

    // Get the number of expansion symbols on the fulcrum encoder
    std::cout << "Expansion symbols on the fulcrum encoder : "
              << encoder.expansion() << std::endl;

    // Get the number of expansion symbols on the fulcrum decoder
    std::cout << "Expansion symbols on the fulcrum decoder : "
              << decoder.expansion() << std::endl;

    // Allocate some storage for a "payload" the payload is what we would
    // eventually send over a network
    std::vector<uint8_t> payload(encoder.max_payload_size());


    // Allocate some data to encode. In this case we make a buffer
    // with the same size as the encoder's block size (the max.
    // amount a single encoder can encode)
    std::vector<uint8_t> data_in(encoder.block_size());

    // Just for fun - fill the data with random data
    std::generate(data_in.begin(), data_in.end(), rand);

    // Assign the data buffer to the encoder so that we may start
    // to produce encoded symbols
    encoder.set_symbols_storage(data_in.data());

    // Define a data buffer where the symbols should be decoded
    std::vector<uint8_t> data_out(decoder.block_size());
    decoder.set_symbols_storage(data_out.data());

    std::vector<uint8_t> data_out_re(decoder.block_size());
    recoder.set_symbols_storage(data_out_re.data());

    // std::vector<uint8_t> data_out_2(recoder.block_size());
    // recoder.set_symbol_storage(data_out_2.data());


    // Generate packets until the decoder is complete
    uint32_t index = 0;
    


    // using file_encoder = kodo_core::object::file_encoder<kodo_rlnc::encoder>;

    // using file_decoder = kodo_core::object::file_decoder<kodo_rlnc::decoder>;

    TimeCounter tcounter;

    // for (uint32_t i = 0; i < encoder.blocks(); ++i)
    // {
    //     file_encoder::stack_pointer e = encoder.build(i);
    //     file_decoder::stack_pointer d = decoder.build(i);
    //     std::vector<uint8_t> payload(e->max_payload_size());

        while (!decoder.is_complete())
        {
            index ++;
            // Encode a packet into the payload buffer
            tcounter.coding.start();
            encoder.produce_payload(payload.data());

            // std::cout << index <<") Encoded packet generated and received by the recoder =>"
            //           << std::endl << std::endl;
            if ((rand() % 10))  // 0 1 2 3 4 5 6 7 8 9 loss=10%
                {
                    continue;
                }

            // recoder.consume_payload(payload.data());

            // recoder.produce_payload(payload.data());
            // std::cout << "Recoded packet generated and received by the decoder =>"
            //           << std::endl << std::endl;
            if (!(rand() % 10)) // 0 1 2 3 loss=10%
                {
                    continue;
                }
            // Pass that packet to the decoder
            decoder.consume_payload(payload.data());
            tcounter.coding.stop();

            
            std::cout << " Encoder and Decoder time is "<< tcounter.coding.time() << " ns" << std::endl;
        }
    // }

        std::cout << "Throughtput is bytes/time = "<< 
                (double)symbols*symbol_size/tcounter.coding.time() << "Gb/s" << std::endl;
    // Check if we properly decoded the data
    if (std::equal(data_out.begin(), data_out.end(), data_in.begin()))
    {
        std::cout << "Data decoded correctly" << std::endl;
    }
    else
    {
        std::cout << "Unexpected failure to decode, "
                  << "please file a bug report :)" << std::endl;
    }
}

