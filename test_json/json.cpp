#include <iostream>
#include <fstream>
// #include "include/json/json.h"
#include <jsoncpp/json/json.h>


using namespace std;

int main() {
    ifstream ifs("profile.json");
    Json::Reader reader;
    Json::Value obj;
    reader.parse(ifs, obj);     // Reader can also read strings
    cout << "Last name: " << obj["lastname"].asString() << endl;
    cout << "First name: " << obj["firstname"].asString() << endl;

    Json::Value through;
    std::vector<double> en;
    en.push_back(1.0);
    en.push_back(2.3);

    double dn[] = {3.4, 4.5};
    for(auto item: dn)
        through["e"].append(item);
    Json::StyledStreamWriter writer;
    std::ofstream tes1("out.json");
    writer.write(tes1,through);    
    return 1;
}

