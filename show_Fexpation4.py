import matplotlib.pyplot as plt
import numpy as np

labels = ['16','32','160','800','1600','8000','16000']

Fulcrum_enc =[[13455.4,11109.3, 3391.32, 612.523, 380.654, 83.2796, 64.0049 ],
              [14839.9, 15347.3, 3683.31, 819.439, 430.528, 91.4952, 69.3601 ],
              [13264.2, 21574.2, 5783.68, 737.204, 454.339,  92.3003, 68.3472] ,
              [16988.9, 12092.8, 2284.9,  635.024, 440.895, 88.6288, 60.727]]

Fulcrum_dec=[[4224.76, 3139.6,  964.139, 185.095, 112.537, 18.1618, 10.7553 ],
             [4689.53, 3715.2,  1141.03, 270.923, 139.083, 20.9001, 11.6094 ],
             [7704.95, 5910.37, 1725.56, 269.8,   142.451,  20.422,  11.2033 ],
             [7404.74, 4385.34, 898.849, 218.577, 142.183, 21.2811, 10.4423]]



x = np.arange(len(labels))
width = 0.2

fig, ax = plt.subplots(2,1)

for i in range(4):
    ax[0].bar(x-width*(3-i*2)/2, Fulcrum_enc[i], width,label="Enc Expasion="+str(i+1))
    
    ax[0].set_ylabel('Throughput Mb/s')
    # ax[0].set_title('')
    ax[0].set_xticks(x)
    ax[0].set_xticklabels(labels)
    ax[0].set_xlabel('Packets Numbers')
    ax[0].legend()
    ax[0].grid(True)

    ax[1].bar(x-width*(3-i*2)/2, Fulcrum_dec[i], width,label="Dec Expasion="+str(i+1))
    ax[1].set_ylabel('Throughput Mb/s')
    # ax[1].set_title('')
    ax[1].set_xticks(x)
    ax[1].set_xticklabels(labels)
    ax[1].set_xlabel('Packets Numbers')
    ax[1].grid(True)

    ax[1].legend()


#autolabel(rects1)
#autolabel(rects2)
#autolabel(rects3)
#autolabel(rects4)

fig.tight_layout()
plt.show()