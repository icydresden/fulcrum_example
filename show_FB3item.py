import matplotlib.pyplot as plt
import numpy as np

#labels = ['16','32','160','800','1600','8000','16000']
#Fulcrum_enc = [9.22685,23.1406,6.64565,1.0622, 0.766703,0.124381,0.103992]
#Fulcrum_dec = [6.15359,4.98453,1.49398,0.26819,0.153,   0.0222987,0.0132676]


# labels = ['160','800','1600','8000','16000'] # 1 2
labels = ['1600','8000','16000'] # 1 2


#Fulcrum_enc = [6.64565,1.0622, 0.766703,0.124381,0.103992] # 1
#Fulcrum_dec = [1.49398,0.26819,0.153,   0.0222987,0.0132676] # 1


#Fulcrum_enc=[12.5497 ,14.9192, 4.90376,1.03994,0.771566,0.136066,0.0947331] #2
#Fulcrum_dec=[4.78841,3.40807,1.02972,0.253839,0.159669,0.0238222,0.0126569] #2
# Fulcrum_enc=[ 4.90376,1.03994,0.771566,0.136066,0.0947331] #2
# Fulcrum_dec=[1.02972,0.253839,0.159669,0.0238222,0.0126569] #2

Fulcrum_enc=[771.566,136.066,94.7331] #3
Fulcrum_dec=[159.669,23.8222,12.6569] #3

BATS_enc = [338.166,274.536,226.881]
BATS_dec = [15.4395,8.30295,4.55196]

x = np.arange(len(labels))
width = 0.2

fig, ax = plt.subplots()

rects1 = ax.bar(x-width*3/2, Fulcrum_enc, width,label="Fulcrum_enc")
rects2 = ax.bar(x-width/2, Fulcrum_dec, width,label="Fulcrum_dec")

rects3 = ax.bar(x+width*3/2, BATS_enc, width,label="BATS_enc")
rects4 = ax.bar(x+width/2, BATS_dec, width,label="BATS_dec")

ax.set_ylabel('Throughput Mb/s')
ax.set_title('')
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend()

#autolabel(rects1)
#autolabel(rects2)
#autolabel(rects3)
#autolabel(rects4)

fig.tight_layout()
plt.show()