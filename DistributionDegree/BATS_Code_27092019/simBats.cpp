/*
 Copyright (C) 2014 Shenghao Yang
 Copyright (C) 2019 Yicong Su

 This file is part of SimBATS.
 
 SimBATS is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 SimBATS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with SimBATS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>
#include <sys/times.h>

// #include "Utilities.h"
// #include "BatchEnc.h"
// #include "BatchDec.h"
// //#include "NCCoder.h"
// #include "FiniteField.h"
// #include "simRecoder.h"

// #include "BatchEnc.cpp"
// #include "BatchDec.cpp"
// #include "FiniteField.cpp"


// #include "include/Utilities.h"
// #include "include/BatchEnc.h"
// #include "include/BatchDec.h"
// //#include "NCCoder.h"
// #include "include/FiniteField.h"

// #include "include/simRecoder.h"
#include "include/BatsSimulator.hpp"

#include "include/TimeCounter.hpp"


using namespace std;


int main(int argc, char* argv[]) {

    // set parameters
    int batchSize;
    int gf_order = 8; // 1, 2, 4, 8
    int packetNum;
    int packetSize = 1024; //In Bytes
    //int packetSizeInSymbol = packetSize * SymbolSize / gf_order;

    int iterationNum;
    //const float decRatio = 0.99;


    switch(argc) {
        case 1:
            batchSize = 32; // default 32.  16, 32, 64
            packetNum = 1600; // default 1600
            iterationNum = 10; // default 40
            break;

        case 2:
            batchSize = 32; // default 32.  16, 32, 64
            packetNum = atoi(argv[1]);//1600; // default 1600
            iterationNum = 10; // default 40
            break;
            
        case 4:
            batchSize = atoi(argv[1]);
            packetNum = atoi(argv[2]);
            iterationNum = atoi(argv[3]);
            break;
        default:
            cout << "simbats M K numIteration" << endl;
            return 0;
    }

    BATSimulator sim(batchSize, gf_order, packetNum, packetSize);

    cout << "Simulation starts with " << "M = " << batchSize << ", q = 2^" << gf_order << ", K = " << packetNum << ", T = " << packetSize << endl;

    int iter = 0;

    int errIdx[iterationNum];
    int nSucc = 0;
    int nErrIdx = 0;
    float totalTrans = 0.0;
    TimeUsed timeUsed;
    // TimeCounter tCounter;

    DecoderStatus ds(batchSize);
    double accuRankDist[batchSize + 1];

    for (int i = 0; i < batchSize + 1; i++) {
        accuRankDist[i] = 0;
    }

    ofstream output;
    stringstream iss;
    time_t t = time(0);
    struct tm * now = localtime(&t);

    iss << "simK" << packetNum << "M" << batchSize << "m" << gf_order << "(" << now->tm_mon + 1 << now->tm_mday << now->tm_hour << now->tm_min << now->tm_sec << ").txt";

    output.open(iss.str().c_str());

    while (iter < iterationNum) {
        cout << "===== " << iter << " =====" << endl;
        sim.runOnce(timeUsed, ds);
        iter++;

        output << "ds.nTrans "  << ds.nTrans << " " 
               << "ds.nReceive" << ds.nReceive << " " 
               << "ds.nSave"    << ds.nSave << " " 
               << "ds.nInact"   << ds.nInact << " " 
               << "ds.nError"   << ds.nError << " ";

        cout << "Decoded/Total: " << packetNum - ds.nError << "/" << packetNum << " With " << ds.nTrans << " packets transmitted" << endl;

        double rate = (packetNum - ds.nError) / (float)ds.nReceive;

        cout << "Rate = " << rate << endl;

        output << rate << " ";

        cout << "Eecoding time = " << timeUsed.encoding.time() << " "
             << "Recoding time = " << timeUsed.recoding.time() << " "
             << "Dncoding time = " << timeUsed.decoding.time() << " "
             << endl;



        // cout << "Total time: get_en_de_time() =" << timeUsed.get_en_de_time()<< endl;
        cout << "Rank Distribution: ";

        double Erk = 0.0;

        if (ds.nError == 0) {
            nSucc++;
            totalTrans += ds.nTrans;
        } else {
            errIdx[nErrIdx++] = iter-1;
        }


        for (int i = 0; i <= batchSize; i++) {
            output << ds.rankdist[i] << " ";
            cout << ds.rankdist[i] << " ";
            Erk += i * ds.rankdist[i];
            accuRankDist[i] += ds.rankdist[i];
        }

        output << "\n" << endl;

        cout << endl;

        cout << "E[rank(H)] = " << Erk << "(" << Erk / (float) batchSize << ")" << endl;
    }

    output.close();

    cout << "======== END =======" << endl;
    cout << "Simulation ends with " << nSucc << " succeeds out of " << iter << " runs" << endl;

    cout << "Average Rank Distribution: ";

    double totalRank = 0;
    double sumRank = 0;
    for(int i = 0; i <= batchSize; i++) {
        sumRank += accuRankDist[i];
    }
    for(int i = 0; i <= batchSize; i++) {
        accuRankDist[i] /= sumRank;
        totalRank += i * accuRankDist[i];
        cout << accuRankDist[i] << " ";
    }
    cout << endl;

    cout << "Average rate of succeeded runs: "  << endl
         << "nSucc * packetNum / (float)totalTrans = " << nSucc << " * " << packetNum << "/ " << (float)totalTrans << " = " 
         <<  nSucc * packetNum / (float)totalTrans  << endl
         << "Vs average rank: " << "totalRank " << " / " << "(float) batchSize = " << totalRank << "/" << (float) batchSize << " = "
         << totalRank / (float) batchSize << endl;

    cout << "Error iterations: ";
    for (int i = 0; i < nErrIdx; i++) {
        cout << errIdx[i] << " ";
    }
    cout << endl;


    std::cout << "Encoder Throughtput "
            //   << (double) enc_index*(payload.size())*8*RUN_LOOP/total_en_time << " Gb/s " 
                << ds.nTrans*packetSize*8/(timeUsed.encoding.time()*1000000) << " Mb/s " 
                << "Encodered Packet = " << sim.enc_index*batchSize << "\n"
                << "ds.nTrans = " << ds.nTrans << "\n"

            //     << "Recoder Throughtput "
            // //   << (double) rec_index*(payload.size())*8*RUN_LOOP/total_re_time << " Gb/s "
            //     << ds.nTrans*packetSize*8/(timeUsed.recoding.time()*1000000) << " Mb/s "  

            //     << "Recodered Packet = " << sim.rec_index << "\n"

                << "Decoder Throughtput "
            //   << (double) dec_index*(payload.size())*8*RUN_LOOP/total_de_time << " Gb/s "
                << nSucc*packetSize*8/(timeUsed.decoding.time()*1000000) << " Mb/s "  

                << "Decodered Packet = " << sim.dec_index*batchSize << std::endl

                << "nSucc ="  << nSucc 
                << std::endl;
}
